<?php
/**
 * Menu.php file
 * Required
 * - Add Menu entries to other modules
 *
 * @package VLaby module
 */

// Use dgettext() function instead of _() for Module specific strings translation.
// See locale/README file for more information.

// Add a Menu entry to the Resources module.
if ( $RosarioModules['Resources'] ) // Verify Resources module is activated.
{
	$menu['Resources']['admin']['VLaby/VLaby.php'] = dgettext( 'VLaby', 'VLaby' );

    $menu['Resources']['teacher']['VLaby/VLaby.php'] = dgettext( 'VLaby', 'VLaby' );

	$menu['Resources']['parent']['VLaby/VLaby.php'] = dgettext( 'VLaby', 'VLaby' );
}
