<?php
/**
 * VLaby
 *
 * @package VLaby module
 */

require_once 'classes/curl.php';
require_once 'ProgramFunctions/TipMessage.fnc.php';
require_once 'modules/VLaby/includes/VLaby.fnc.php';

if ( $_REQUEST['modfunc'] === 'login' )
{
	if ( VLabyLogin( issetVal( $_REQUEST['email'], '' ), issetVal( $_REQUEST['password'], '' ) ) )
	{
		// Now we can get the user experiments list.
		// Redirect.
		$experiments_url = 'Modules.php?modname=' . $_REQUEST['modname'] . '&modfunc=experiments&LO_sort=GRADE_LEVEL';
		?>
		<script>ajaxLink(<?php echo json_encode( URLEscape( $experiments_url ) ); ?>);</script>
		<?php

		exit;
	}

	// Remove modfunc & redirect URL.
	RedirectURL( 'modfunc' );
}

if ( $_REQUEST['modfunc'] === 'experiments' )
{
	DrawHeader( ProgramTitle() );

	$experiments_url = 'https://vlaby.com/api/user/experiments';

	$experiments = VLabyAPIRequest( $experiments_url );

	if ( $experiments )
	{
		$experiments = $experiments['experiments'];

		//var_dump($experiments);

		$experiments_list = VLabyGetExperimentsList( $experiments );

		$LO_columns = [
			'TITLE' => _( 'Title' ),
			'SUBJECT' => _( 'Subject' ),
			'POINTS' => _( 'Points' ),
			'COUNTRY' => dgettext( 'VLaby', 'Country' ),
			'GRADE_LEVEL' => _( 'Grade Level' ) . ' - ' . _( 'Marking Period' ),
		];

		ListOutput(
			$experiments_list,
			$LO_columns,
			dgettext( 'VLaby', 'Experiment' ),
			dgettext( 'VLaby', 'Experiments' )
		);
	}

	echo ErrorMessage( $error );
}

if ( $_REQUEST['modfunc'] === 'experiment' )
{
	DrawHeader( ProgramTitle() );

	$experiment_url = 'https://vlaby.com/api/experiment/' . $_REQUEST['id'];

	$experiment = VLabyAPIRequest( $experiment_url );

	if ( $experiment )
	{
		//var_dump($experiment);

		$experiment = $experiment['experiment'];

		if ( ! empty( $experiment['file'] ) )
		{
			DrawHeader( $experiment['title'] );

			echo '<iframe src="' . URLEscape( $experiment['file'] ) . '" style="width: 100%; height: calc(100vh - 150px);"></iframe><br />';

			DrawHeader( $experiment['title'] );

			DrawHeader( $experiment['subject_name'], $experiment['country_name'] );

			DrawHeader( $experiment['level_name'] . ' - ' . $experiment['level_class_name'], $experiment['semester_name'] );

			echo '<br />' . $experiment['description'];
		}
	}

	echo ErrorMessage( $error );
}

if ( ! $_REQUEST['modfunc'] )
{
	DrawHeader( ProgramTitle() );

	echo ErrorMessage( $error );

	echo '<form action="' . URLEscape( 'Modules.php?modname=' . $_REQUEST['modname'] . '&modfunc=login' ) . '" method="POST">';

	PopTable( 'header', sprintf( _( '%s Login' ), dgettext( 'VLaby', 'VLaby' ) ) );

	$lang_2_chars = mb_substr( $_SESSION['locale'], 0, 2 );

	$available_langs = [ 'en', 'fr', 'de', 'ar', 'id' ];

	if ( ! in_array( $lang_2_chars, $available_langs ) )
	{
		// Default lang to 'en'.
		$lang_2_chars = 'en';
	}

	?>
	<table class="cellspacing-0 cellpadding-5 width-100p">
		<tr>
			<td>
				<label>
					<input type="text" name="email" id="email" size="30" maxlength="100" required autofocus />
					<br /><?php echo _( 'Email' ); ?>
				</label>
			</td>
		</tr>
		<tr>
			<td>
				<label>
					<input type="password" name="password" id="password" size="30" maxlength="42" required />
					<br /><?php echo _( 'Password' ); ?>
				</label>
				<div class="align-right">
					<a href="https://vlaby.com/<?php echo $lang_2_chars; ?>/password/reset" rel="nofollow" target="_blank">
						<?php echo _( 'Password help' ); ?>
					</a>
				</div>
			</td>
		</tr>
	</table>
	<br /><div class="center"><?php echo Buttons( _( 'Login' ) ); ?></div>
	<?php

	PopTable( 'footer' );

	echo '</form>';
}
