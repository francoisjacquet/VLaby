VLaby module
============

![screenshot](https://gitlab.com/francoisjacquet/VLaby/raw/master/screenshot.png?inline=false)

https://gitlab.com/francoisjacquet/VLaby/

Version 1.0 - September, 2023

License GNU/GPLv2 or later

Author François Jacquet

Sponsored by Haitem Bensaid, Libya

DESCRIPTION
-----------
Browse and access the VLaby experiments from within RosarioSIS.

Experiments are available for various countries and grade levels, for the following subjects:
- Biology
- Chemistry
- Physics

VLaby is available in English, Arabic, French, German & Indonesian.

Translated in French and Spanish.

More information about VLaby here: https://vlaby.com/

CONTENT
-------
Resources
- VLaby

INSTALL
-------
Copy the `VLaby/` folder (if named `VLaby-master`, rename it) and its content inside the `modules/` folder of RosarioSIS.

Go to _School > Configuration > Modules_ and click "Activate".

Requires RosarioSIS 6.8+
