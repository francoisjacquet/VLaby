<?php
/**
 * VLaby functions
 *
 * @package VLaby module
 */

/**
 * VLaby API request
 *
 * @global $error Add error message(s) if any
 *
 * @param string $url  API URL.
 * @param array  $data POST data (optional).
 *
 * @return bool|array False if error, else decoded $response JSON array, $response['data']
 */
function VLabyAPIRequest( $url, $data = [] )
{
	global $error;

	$curl = new curl(); // [ 'debug' => ROSARIO_DEBUG ]

	//$curl->setHeader( 'Content-Type: application/json' );

	if ( ! empty( $_SESSION['VLaby']['token'] ) )
	{
		// Send our session token in the Authorization HTTP header.
		$curl->setHeader( 'Authorization: Bearer ' . $_SESSION['VLaby']['token'] );
		//$curl->setHeader( 'X-Auth-Token: Bearer ' . $_SESSION['VLaby']['token'] );
	}

	$lang_2_chars = mb_substr( $_SESSION['locale'], 0, 2 );

	$available_langs = [ 'en', 'fr', 'de', 'ar', 'id' ];

	if ( ! in_array( $lang_2_chars, $available_langs ) )
	{
		// Default lang to 'en'.
		$lang_2_chars = 'en';
	}

	// Send our preferred language in the locale header.
	$curl->setHeader( 'locale: ' . $lang_2_chars );

	if ( $data )
	{
		$response = $curl->post( $url, $data );
	}
	else
	{
		$response = $curl->get( $url );
	}

	$decoded = json_decode( $response, true );

	if ( json_last_error() === JSON_ERROR_NONE )
	{
		$response = $decoded;
	}

	if ( empty( $response['status'] ) )
	{
		if ( empty( $response['errors'] )
			&& empty( $response['msg'] ) )
		{
			$error[] = "VLaby API error: URL " . $url . " - Code " . $curl->info['http_code'];

			return false;
		}

		if ( empty( $response['errors'] ) )
		{
			$error[] = $response['msg'];

			return false;

		}

		foreach ( (array) $response['errors'] as $error_msg )
		{
			if ( is_array( $error_msg ) )
			{
				$error[] = implode( ' - ', $error_msg );
			}
			else
			{
				$error[] = $error_msg;
			}
		}

		return false;
	}

	return ! empty( $response['data'] ) ? $response['data'] : $response;
}

/**
 * VLaby login, & get session token.
 *
 * Sets $_SESSION['VLaby']['token']
 *
 * @uses VLabyAPIRequest()
 *
 * @param string $email    Email address.
 * @param string $password Password.
 *
 * @return bool False if not logged in.
 */
function VLabyLogin( $email, $password )
{
	global $error;

	if ( ! $email
		|| ! $password )
	{
		return false;
	}

	$auth_url = 'https://vlaby.com/api/login';

	$response = VLabyAPIRequest( $auth_url, [
		'email' => $email,
		'password' => $password,
	] );

	if ( ! $response )
	{
		// We had an error, unset VLaby session just in case.
		unset( $_SESSION['VLaby'] );

		return false;
	}

	if ( empty( $response['user']['token'] ) )
	{
		// We had an error, unset VLaby session just in case.
		unset( $_SESSION['VLaby'] );

		$error[] = "VLaby API error: Can't get session token";

		return false;
	}

	// Save email & passqword in session in case token expires.
	//$_SESSION['VLaby']['email'] = $_REQUEST['email'];
	//$_SESSION['VLaby']['password'] = $_REQUEST['password'];

	// User is logged, grab session token.
	$_SESSION['VLaby']['token'] = $response['user']['token'];

	return true;
}


function VLabyGetExperimentsList( $experiments )
{
	if ( ! $experiments )
	{
		return [];
	}

	$experiments_list = [ 0 => [] ];

	foreach ( (array) $experiments as $experiment )
	{
		/*if ( empty( $experiment['status'] ) )
		{
			continue;
		}*/

		if ( empty( $experiment['title'] ) )
		{
			continue;
		}

		$experiments_list[] = [
			'ID' => $experiment['id'],
			'TITLE' => VLabyMakeExperimentTitle( $experiment ),
			'SUBJECT' => $experiment['subject_name'],
			'POINTS' => $experiment['points'],
			'COUNTRY' => $experiment['country_name'],
			'GRADE_LEVEL' => $experiment['level_name'] . ' - ' . $experiment['level_class_name'] . ' - ' . $experiment['semester_name'],
		];

	}

	unset( $experiments_list[0] );

	return $experiments_list;
}

function VLabyMakeExperimentTitle( $experiment )
{
	$experiment_url = 'Modules.php?modname=' . $_REQUEST['modname'] .
		'&modfunc=experiment&id=' . $experiment['id'];

	$tip_msg = MakeTipMessage(
		'<img src="' . URLEscape( $experiment['image'] ) . '" loading="lazy" width="270">',
		_( 'Icon' ),
		$experiment['title']
	);

	return '<a href="' . URLEscape( $experiment_url ) . '">' . $tip_msg . '</a>';
}
